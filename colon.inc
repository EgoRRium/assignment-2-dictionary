%define elem 0
%macro colon 2
    %2:
    dq elem
    db %1, 0
    %define elem %2
%endmacro
