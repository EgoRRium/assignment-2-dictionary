section .text

global find_word
extern string_equals

find_word:
    push r12              ;Callee-saved
    mov r12, rdi
    push r13              ;Callee-saved
    mov r13, rsi
    xor rax, rax
    .loop:
        test r13, r13
        je .end
        lea rsi, [r13+8]
        mov rdi, r12
        call string_equals
        test rax, rax
        jne .end
        mov r13, [r13]
        jmp .loop
    .end:
        mov rax, r13
    pop r13
    pop r12
    ret
