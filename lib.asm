section .data

section .text
%define WRITE 1
%define EXIT 60
%define STDOUT 1
%define STDERR 2

global exit
global string_length
global print_string
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov  rax, EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov  rax, rdi
  .counter:
    cmp  byte [rdi], 0
    je   .end
    inc  rdi
    jmp  .counter
  .end:
    sub  rdi, rax
    mov  rax, rdi
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov  rax, WRITE
    mov  rdi, STDOUT
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline: 
    mov rdi, 0xA
; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    pop rdi
    mov rax, WRITE
    mov rdi, STDOUT
    mov rdx, 1
    syscall
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jge .print
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
  .print:
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint: 
    mov rax, rdi
    mov r8, rsp
    push 0
  .loop:  
    xor rdx, rdx
    mov rdi, 0xA
    idiv rdi
    add rdx, 48
    dec rsp
    mov byte[rsp], dl
    test rax, rax
    jnz  .loop
    mov rdi, rsp
    push r8
    call print_string
    pop rsp
    ret


print_error:
    push rdi
    call string_length
    pop	rsi
    mov	rdx, rax
    mov	rax, WRITE
    mov	rdi, STDERR
    syscall
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    xor r8, r8
    xor r9, r9
  .loop:
    mov r8b, byte[rdi+rcx]
    cmp r8b,byte[rsi+rcx]
    jnz .nequal
    cmp r8b,0
    je .equals
    inc rcx
    jmp .loop    
  .equals:
    mov rax, 1
    ret
  .nequal:
    mov rax,0
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    push rax
    mov rsi, rsp
    mov rdx, 1
    syscall
    cmp rax, -1
    jne .ok
    xor rax, rax
    jmp .end
  .ok:
    mov rax, [rsp]
  .end:
    add rsp, 8
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14
    xor r12, r12
    mov r13, rdi
    mov r14, rsi
  .read:
    cmp r12, r14
    jge .err
    call read_char
    cmp rax, 0x20
    je .mid
    cmp rax, 0x9
    je .mid
    cmp rax, 0xA
    je .mid
    mov [r13 + r12], rax
    test rax, rax
    je .end
    inc r12
    jmp .read
  .err:
    xor rax, rax
    pop r14
    pop r13
    pop r12
    ret
  .mid:
    test r12, r12
    jz .read
  .end:
    mov rax, r13
    mov rdx, r12
    pop r14
    pop r13
    pop r12
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:                    
    xor rax, rax
    mov rax, 0
    mov rcx, 0
    mov rdx, 0
.loop:
    mov dl, byte[rdi + rcx]
    cmp dl, '0'
    jb .err
    cmp dl, '9'
    ja .err
    imul rax, 10
    sub rdx, 48
    add rax, rdx
    inc rcx
    jmp .loop
    ret
.err:
    mov rdx, rcx
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov rcx, rdi 
    cmp byte[rcx], '-'
    je .neg
    cmp byte[rcx], '+'
    je .pos
    mov rdi, rcx
    jmp parse_uint
  .pos:
    inc rcx
    mov rdi, rcx
    jmp parse_uint
  .neg:
    inc rcx
    mov rdi, rcx
    push rcx
    call parse_uint
    pop rcx
    neg rax
    inc rdx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
  .loop:
    cmp rax, rdx
    jge .bigbuf
    mov r8b, byte[rdi+rax]
    mov byte[rsi+rax], r8b
    cmp r8b, 0
    je .end
    inc rax
    jmp .loop
  .bigbuf:
    xor rax, rax
  .end:
    ret

