%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define STDIN 0
%define STDOUT 1
%define BUFFER_SIZE 256

section .bss
buffer: resb BUFFER_SIZE

section .rodata
	not_exist_error: db 'Error: key is not exist', 0
	overflow_error: db 'Error: key is longer 255', 0

section .text
global _start
_start:
	mov rdi, buffer
	mov rsi, BUFFER_SIZE
	call read_word
	test rax, rax
	je .fail_length
	
	push rdx

	mov rdi, rax
	mov rsi, elem	
	call find_word
	test rax, rax
	je .fail_exists
	
	pop rdx

	add rax, 9
	mov rdi, rax
	add rdi, rdx
	mov rsi, STDOUT
	call print_string
	jmp .exit
	
	.fail_length:
		mov rdi, overflow_error
		call print_error
		mov rdi, 1
		call exit

	.fail_exists:
		mov rdi, not_exist_error
		call print_error
		mov rdi, 1
		call exit

	.exit:
		xor rdi, rdi
		call exit
