ASM=nasm
FLG=-felf64
PY=python3
LD=ld


.PHONY: test clean


program: main.o dict.o lib.o
	$(LD) -o $@ $^

%.o: %.asm
	$(ASM) $(FLG) -o $@ $<

main.o: main.asm words.inc lib.inc dict.inc
dict.o: dict.asm lib.inc

test: 
	$(PY) test.py

clean:
	rm -f *.o program
