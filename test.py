import subprocess

stdin = ["third", "second", "first", "key does not exist", "", "3"*256]

stdout = ["third word explanation", "second word explanation", "first word explanation with numbers", "Error: key is not exist", "Error: key is not exist", "Error: key is longer 255"]

print("Start testing...")
results = list()
for index, (inp, out) in enumerate(zip(stdin, stdout), start=1):
    print("------------------------------------\n")
    print("Test #" + str(index))
    process = subprocess.Popen(["./program"],
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE,
                               universal_newlines=True)

    output, error = process.communicate(inp)
    if output == "":
        result = error.strip() == out
    else:
        result = output.strip() == out
    results.append("." if result else "F")

    print("Input: " + inp)
    print("Expected: " + out)
    print("Output: " + output)
    print("Error: " + error)
    print("Test passed!" if result else "Test failed!")
    process.terminate()


print_res = "".join(results)
print("------------------------------------\n")
print("Results: " + print_res)
print("All tests passed" if all(i == '.' for i in results) else "Tests failed")
